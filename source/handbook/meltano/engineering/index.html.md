---
layout: handbook-page-toc
title: "Meltano Engineering Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Code

See the active project here: [gitlab.com/meltano/meltano](https://gitlab.com/meltano/meltano)

Read our documentation here: [https://meltano.com/docs/](https://meltano.com/docs/)

## Releases

Meltano is released weekly on Mondays, and follows our [documented release procedure](https://meltano.com/docs/contributing.html#releases)

## Triage process

The `flow::Triage` label is used on issues that need product/prioritization triage by the Product Manager (Danielle), or engineering/assignment triage by the Engineering Lead (Douwe).
After they've been triaged, they'll have a milestone (other than `Backlog`), an assignee, and the `flow::To Do` label.

If you come across something that needs fixing:

1. Create an issue describing the problem.
2. If it's not obvious, justify how it relates to our persona and how it contributes to MAUI.
3. Then:

    - If it's more urgent (has a higher impact on MAUI) than other things you've been assigned, assign it to yourself to work on later the same week:

      ```md
      /milestone %<current milestone>
      /label ~"flow::To Do"
      /reassign @<yourself>
      /cc @DouweM
      ```

    - If it's urgent, but you're not sure who should work on it, assign it to Douwe to triage:

      ```md
      /milestone %<current milestone>
      /label ~"flow::Triage"
      /reassign @DouweM
      ```

    - If it's _not_ urgent or you're unsure whether it's something we should do at all, assign it to Danielle to triage:

      ```md
      /milestone %“Backlog" or %<next milestone>
      /label ~"flow::Triage"
      /reassign @dmor
      ```

## Useful issue boards

- [Development Flow](https://gitlab.com/groups/meltano/-/boards/536761), with a column for each `flow::` label. Don't forget to filter by milestone, and/or assignee!
- [Team Assignments](https://gitlab.com/groups/meltano/-/boards/1402405), with a column for each team member. Don't forget to filter by milestone!
- [Current Milestone](https://gitlab.com/groups/meltano/-/boards/1288307), with a column for each `flow::` label _and_ each team member.
- [Next Milestone](https://gitlab.com/groups/meltano/-/boards/1158410), with a column for each `flow::` label _and_ each team member.
